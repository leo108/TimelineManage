var inputs = $(".controls input");

var checkPwd = function(){
	if(this.value !== "" && this.value !== $("#pwdA")[0].value ){
		$(this).next("span").text("密码不一致").show();
		return false;
	}
	return true;
};

var checkInput = function() {
	var flag = true;
	inputs.each(function(index) {
		if (this.value == "") {
			$(this).next("span").css("display", "inline");
			flag = false;
		}
	});
	var B = $("#pwdB");
	if(B && flag){
		flag = checkPwd.call(B[0]);
	}
	return flag;
};
var inputLinsterKeydown = function() {
	var tip = $(this).next();
	if (tip) {
		tip.hide();
	}
};

var inputLinsterBlur = function() {
	var tip = $(this).next();
	this.value == "" ? tip.text("未填写").show() : tip.hide();
};


inputs.on("keydown", inputLinsterKeydown).on("blur", inputLinsterBlur);
$("#pwdB").on("blur",checkPwd);