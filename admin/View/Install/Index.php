<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>install</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link href="../static/css/bootstrap.css" media="all" rel="stylesheet" type="text/css" />
    <style>
        h1{
            position: absolute;
            top:50px;
            left:10%;
        }
        .tip{
            text-align: center;
            margin-bottom: 30px;
        }
        .content{
            position: absolute;
            top: 140px;
            left: 25%;
        }
        .submit-btn{
            text-align: right;
        }
        .text-error{
            display: none;
        }
    </style>
    <script type="text/javascript" src="../static/js/jquery.js"></script>
    <script type="text/javascript" src="../static/js/bootstrap.min.js"></script>
</head>
<body>
<h1>Timeline安装</h1>
<div class="content">
  <div class="tip">请在下方填写您的数据库连接信息。如果您不确定，请联系您的服务提供商。</div>
  <form class='form-horizontal' id='installForm' method="post" action="?c=Install&a=Install">
    <div class="control-group">
      <label class="control-label">数据库主机</label>
      <div class="controls">
        <input type="text" name='DB_HOST' value='localhost'/>
        <span class="text-error"> 未填写</span>
      </div>
    </div>
    <div class="control-group">
      <label class="control-label">数据库端口</label>
      <div class="controls">
        <input type="text" name='DB_PORT' value='3306'/>
        <span class="text-error"> 未填写</span>
      </div>
    </div>
    <div class="control-group">
      <label class="control-label">数据库名</label>
      <div class="controls">
        <input type="text" name='DB'/>
        <span class="text-error"> 未填写</span>
      </div>
    </div>
    <div class="control-group">
      <label class="control-label">数据库用户名</label>
      <div class="controls">
        <input type="text" name='DB_USER'/>
        <span class="text-error"> 未填写</span>
      </div>
    </div>
    <div class="control-group">
      <label class="control-label">数据库密码</label>
      <div class="controls">
        <input type="password" name='DB_PWD'/>
        <span class="text-error"> 未填写</span>
      </div>
    </div>
    <div class="control-group">
        <label class="control-label">管理员用户名</label>
        <div class="controls">
            <input type="text" name='USER'/>
            <span class="text-error"> 未填写</span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">管理员密码</label>
        <div class="controls">
            <input type="password" id="pwdA" name='PWDONCE'/>
            <span class="text-error"> 未填写</span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">再次输入管理员密码</label>
        <div class="controls">
            <input type="password" id="pwdB" name='PWDTWICE'/>
            <span class="text-error"> 未填写</span>
        </div>
    </div>
    <div class="submit-btn"><button id="enter" type="submit" class="btn btn-default btn-lg" onclick="return checkInput();">确 定</button></div>
  </div>
  <script src="../static/js/install.js"></script>
</body>
</html>