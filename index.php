<?php
include './SinglePHP.class.php';
$config = array(
    'APP_PATH' => './main/',
    'DB_HOST' => getenv('MOPAAS_MYSQL15066_HOST'),
    'DB_PORT' => getenv('MOPAAS_MYSQL15066_PORT'),
    'DB_USER' => getenv('MOPAAS_MYSQL15066_USER'),
    'DB_PWD' => getenv('MOPAAS_MYSQL15066_PASSWORD'),
    'DB_NAME' => getenv('MOPAAS_MYSQL15066_NAME'),
    'DB_CHARSET' => 'utf8',
);
SinglePHP::getInstance($config)->run();
