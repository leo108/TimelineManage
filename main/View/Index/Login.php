<!DOCTYPE html><head>
<title>芝麻开门</title>
<meta charset='UTF-8'>
<link href="/static/css/bootstrap.css" media="all" rel="stylesheet" type="text/css" />
<link href="/static/css/bootstrap-responsive.css" media="all" rel="stylesheet" type="text/css" />
<link href="/static/css/docs.css" media="all" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/static/js/jquery.js"></script>
<script type="text/javascript" src="/static/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/static/js/bootstrap.min.js"></script>
<style type="text/css">
.line-large {
padding-top: 20px;
}
.input-prepend{
    padding-top: 150px;
}
</style>
</head>
<body>
    <div class="container">
        <div class="row-fluid">
            <div class="span12">
                <center>
                    <div class="input-prepend">
                      <span class="add-on"><i class="icon-lock"></i></span>
                      <input id="prependedInput" type="text" placeholder="密码">
                  </div>
                  <div class='line-large'></div>
                  <div class='line-large'></div>
                  <div>
                    <button class="btn btn-large btn-primary" type="button" >登入</button>
                </div>
            </center>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('button').click(function(){
        $.cookie('pwd',$('#prependedInput').val(),{expires:365});
        location.reload();
    });
});
</script>
</body></html>