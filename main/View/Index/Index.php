<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>timeline</title>
        <style>
            html, body {
                height:100%;
                padding: 0px;
                margin: 0px;
            }
        </style>
<script type="text/javascript" src="./static/js/jquery.js"></script>
<script type="text/javascript" src="./static/js/jquery.cookie.js"></script>
<script type="text/javascript" src="./static/js/storyjs-embed.js"></script>
    </head>

    <body>
        <!-- BEGIN Timeline Embed -->
        <div id="timeline-embed"></div>
        <script type="text/javascript">
            $(document).ready(function(){
                flag = $.cookie('flag');
                timeline_config = {
                    width:  "100%",
                    height: "100%",
                    source: '/index.php?c=Index&a=Data',
                    //debug:  true,
                    lang:   'zh-cn',
                    hash_bookmark: true
                }
                if(typeof(flag) == 'undefined'){
                    timeline_config['start_at_end'] = false;
                    $.cookie('flag','ok',{expires:365});
                }
                createStoryJS(timeline_config);
            });
        </script>

        <!-- END Timeline Embed -->
    </body>
</html>
